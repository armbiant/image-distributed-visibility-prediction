import h5py
import sys
import numpy
import itertools

viss = [ h5py.File(fname, 'r')['vis'] for fname in sys.argv[1:] ]

# Determine number of antennas and time/frequency slots
uvw = viss[0]['uvw']
time = viss[0]['time']
freq = viss[0]['frequency']
nant = uvw.shape[0]
ntime = time.shape[0]
nfreq = freq.shape[0]

print(f"{nant} antennas, {ntime} time steps, {nfreq} channels")
all_vis = numpy.empty((len(viss), ntime, nfreq, 1), dtype=complex)

# Now loop through antennas and channels
nvis = 0; nbl = 0
for a1 in range(nant):
    for a2 in range(a1+1,nant):
        for i, vis in enumerate(viss):
            vis[str(a1)][str(a2)]['vis'].read_direct(all_vis[i])
        nset = (all_vis != 0).sum(axis=0)
        if numpy.max(nset) > 1:
            pos = numpy.unravel_index(numpy.argmax(nset), nset.shape)
            print(f"BL {a1}/{a2}: visibility {pos} set twice!")
            for i in range(len(viss)):
                print(f"{sys.argv[1+i]}: {all_vis[(i,*pos)]}")
        if numpy.min(nset) < 1:
            for it in range(ntime):
                pos = numpy.unravel_index(numpy.argmin(nset[it]), nset[it].shape)
                if nset[it][pos]:
                    continue
                ifr = pos[0]
                ip = pos[1]
                u,v,w = (uvw[a2][it] - uvw[a1][it]) * freq[ifr] / 299792458

                groups = []
                i = 0
                for _v, grp in itertools.groupby(nset[it]):
                    grp_len = sum( 1 for _x in grp )
                    if _v == 0:
                        groups.append(f"{i}:{i+grp_len}")
                    i += grp_len
                i = numpy.unravel_index(numpy.argmin(nset[it]), nset[it].shape)
                print(f"BL {a1}/{a2}: visibility t={it} f={','.join(groups)} p={ip} (uvw={u:.2f}/{v:.2f}/{w:.2f}) not set!")        
        nvis += numpy.sum(nset != 0)
        nbl += 1

print(f"{nvis}/{nant*(nant-1)//2*ntime*nfreq} visibilities found")
