
Running GitLab CI jobs locally
=============================================

Context
--------

Often the runners are busy and we tend to wait for really long time before the CI job is executed. I have also come across times where I have to test .gitlab-ci.yml file _perse_ rather than the changes of the source code. It was quite annoying to commit and push the code everything before having to run the job.

This little script can solve some of these issues by running CI jobs locally. This is especially helpful to check the typos and verify the logic (with some limitations) in the CI config file. **Important** things to remember

- We should commit the changes to source code to test those changes in CI job. However, there is no need to push the changes to remote. This way we can simply squash or rebase those iterative commits into one before pushing it to remote.
- There is **no need** to commit changes to .gitlab-ci.yml to test them in the CI job.
- We can only run one job at a time. Currently, there is no way that we can run all CI jobs as it does online.

As told, there are limitations on what can be done using this script. More information can be found on [GitLab](https://docs.gitlab.com/runner/commands/) documentation in the section "Limitations of `gitlab-runner exec`". There is ongoing [issue](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/2797) as well to add a full-fledged local CI runner feature to the GitLab.

Looking into the comments of the issue, I found this [repository](https://github.com/firecow/gitlab-ci-local) on GitHub where the author claims that this code can run entire CI file locally without having to commit the changes.

Usage
------

The only requirement to run this script to have docker installed on the local machine. Usage of the script is pretty straight-forward. Copy the files `gitlab-local-runner.sh` and `gitlab-runner.conf` to the root of the repository. Populate the `gitlab-runner.conf` with all the configuration information needed to run the jobs. This includes personal credentials, meta data of the repository, _etc_.

The script can be run as follows:

```
./gitlab-local-runner.sh <job_name>
```
where `<job_name>` is name of the job in CI config file. For example, if we want to test a job called `build`, we can do it using `./gitlab-local-runner.sh build`

What script does is parse the `gitlab-runner.conf`, where variables are passed to runner as environment variables. It checks if there is a running `gitlab-runner` container. If not found, it pulls the image and starts a container.

Simple as that. It can come handy in some situations. 
