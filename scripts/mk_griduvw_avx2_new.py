import sys
import os
import re
from pathlib import Path

if len(sys.argv) != 2:
    print("Please supply an output file name!", file=sys.stderr)
    exit(1)

# Extract desired kernel size from file name
out_fname = sys.argv[1]
match = re.search('([0-9]+)_([0-9]+)(\+([0-9]+))?\.', os.path.basename(out_fname))
kernel_size = int(match[1])
wkernel_size = int(match[2])
wphase = match[4]
if wphase is not None:
    wphase = int(wphase)

# Architecture assumptions (bytes)
double_size = 8
double_complex_size = 16
register_size = 32
cache_line_size = 64
sse_align = 16
avx_align = 32

# Algorithm variants
kernel_z_load = True
unroll_z_loop = True
kernel_y_load = True
unroll_y_loop = True
prepermute_y = False
prepermute_x = False
interleave_wplanes = (wphase is not None)

# Number of parallel sums. Should be at least the number of parallel
# FMA units present on the CPU core
parallel_sums = 2

# No overflow for the moment
assert (kernel_size * double_complex_size % register_size == 0)
assert (wkernel_size * double_complex_size % register_size == 0)

# Needs to be in sync with assumption in load_sep_kern!
kernel_stride = avx_align * ((kernel_size * double_size + avx_align - 1) // avx_align) // double_size
wkernel_stride = avx_align * ((wkernel_size * double_size + avx_align - 1) // avx_align) // double_size

kernel_regs = (kernel_size * double_size + register_size - 1) // register_size
kernel_regs_dup = (kernel_size * double_complex_size + register_size - 1) // register_size
kernel_regs_quad = (kernel_size * double_complex_size * 2 + register_size - 1) // register_size
wkernel_regs = (wkernel_size * double_size + register_size - 1) // register_size
wkernel_regs_dup = (wkernel_size * double_complex_size + register_size - 1) // register_size

# Open the file
Path(out_fname).parent.mkdir(parents=True, exist_ok=True)
out_file = open(out_fname, "w")
def emit(*args, **kwargs):
    print(*args, **kwargs, file=out_file)

# Generate prelude. We assume that conjugation can be achieved using
# an "xor" operation (we essentially just flip a sign).
emit(f"""

// THIS IS A GENERATED FILE!

if( kernel->size == {kernel_size} &&  wkernel->size == {wkernel_size} &&
    kernel->stride == {kernel_stride} && wkernel->stride == {wkernel_stride} && 
    (uintptr_t)kernel->data % {avx_align} == 0 && (uintptr_t)pv % {sse_align} == 0 &&""")

# Check that strides and grid pointers are okay for what we are about to do
if interleave_wplanes:
    emit(f"""    grid_u_stride == {wkernel_size} && grid_w_roll == {wphase} && uvgrid &&
    (uintptr_t)pv % {register_size} == 0 && (grid_v_stride * {double_complex_size}) % {register_size} == 0) {{""")
else:
    emit("    grid_u_stride == 1) {")

emit(f"""

    const int over = kernel->oversampling, over_w = wkernel->oversampling;
    __m128d conj_mask = conjugate ? _mm_xor_pd(_mm_set_pd(-1, 1), _mm_set_pd(1, 1)) : _mm_set_pd(0., 0.);
""")
    
debug_output = False
def emit_print_cpx(ind, var, ix):
    if debug_output:
        emit(f'{ind}printf(" %.4g%+.4gj", {var}[{ix}], {var}[{ix+1}]);')

def emit_print_cpx_reg(ind, var):
    emit_print_cpx(ind, var, 0)
    emit_print_cpx(ind, var, 2)

def calc_frac_coord(ind,u,v,w):
    # Determine oversampling, and calculate first fractional coordinates
    emit(f"""
{ind}// Calculate grid and sub-grid coordinates
{ind}frac_coord_sep_uvw({wkernel_size if interleave_wplanes else 1}, grid_v_stride, {kernel_size}, {kernel_stride}, over, {wkernel_size}, {wkernel_stride}, over_w,
{ind}                   {u},{v},{w}, &next_grid_offset, &next_plane_offset, &sub_offset_x, &sub_offset_y, &sub_offset_z);
""")
    if debug_output:
        emit(f'{ind}printf("u: %g v: %g w: %g  go: %d xo: %d yo: %d zo: %d\\n", u, v, w, next_grid_offset, sub_offset_x, sub_offset_y, sub_offset_z);\n')

emit("    int next_grid_offset, next_plane_offset, sub_offset_x, sub_offset_y, sub_offset_z;")
calc_frac_coord('    ', 'u','v','w')

def copy_kernel(ind, in_ptr, out_ptr, regs):
    """ Copy kernel from and to memory """
    for s in range(regs):
        off = s * register_size // double_size
        emit(f"{ind}_mm256_store_pd({out_ptr}+{off}, _mm256_load_pd({in_ptr}+{off}));")

def load_kernel(ind, in_ptr, out_vars, regs, declare=False):
    """Load kernels into variables (registers, possibly)"""

    dec = ("__m256d " if declare else "")
    for s in range(regs):
        off = s * register_size // double_size
        emit(f"{ind}{dec}{out_vars[s]} = _mm256_loadu_pd({in_ptr}+{off});")

def make_permute(a,b,c,d):
    assert(all([x >= 0 for x in [a,b,c,d]]))
    assert(all([x < register_size // double_size for x in [a,b,c,d]]))
    return (a << 0) | (b << 2) | (c << 4) | (d << 6)

def load_kernel_dup(ind, in_ptr, out_vars, regs, declare=False, shift=0):
    """Load kernels into variables (registers, hopefully), duplicating all
    values in the process."""

    dec = ("__m256d " if declare else "")
    for s in range(regs):
        off = s * register_size // double_size
        permut1 = make_permute((0+shift)%4,(0+shift)%4,(1+shift)%4,(1+shift)%4)
        permut2 = make_permute((2+shift)%4,(2+shift)%4,(3+shift)%4,(3+shift)%4)
        emit(f"{ind}__m256d k{s} = _mm256_loadu_pd({in_ptr}+{off});")
        emit(f"{ind}{dec}{out_vars[2*s]} = _mm256_permute4x64_pd(k{s},{permut1});")
        if 2*s+1 < kernel_regs_dup:
            emit(f"{ind}{dec}{out_vars[2*s+1]} = _mm256_permute4x64_pd(k{s},{permut2});")

def prepermute_kernel(ind, in_vars, out_vars, regs, declare=False):
    """Represent kernel as a series of vectors, each having exactly one kernel value.

    Needs a lot of space, but does not require any permuting later.
    """

    dec = ("__m256d " if declare else "")
    for s in range(regs):
        for v in range(register_size // double_size):
            if 4*s+v < kernel_regs_quad:
                permut = make_permute(v,v,v,v)
                emit(f"{ind}{dec}{out_vars[4*s+v]} = _mm256_permute4x64_pd({in_vars[s]},{permut});")

def halfpermute_kernel(ind, in_vars, out_vars, regs, declare=False):
    """Represent kernel as a series of vectors, each having two kernel
    values that can "standard" permuted using _mm256_permute_pd into a
    form where all four values are the same. Basically trying to get
    around using _mm256_permute4x64_pd in the inner loop.
    """

    dec = ("__m256d " if declare else "")
    for s in range(regs):
        for v in range(register_size // double_size // 2):
            if 2*s+v < kernel_regs_quad:
                permut = make_permute(2*v,2*v+1,2*v,2*v+1)
                emit(f"{ind}{dec}{out_vars[2*s+v]} = _mm256_permute4x64_pd({in_vars[s]},{permut});")

# Load first Z kernel
if not interleave_wplanes:
    if kernel_z_load:
        if not unroll_z_loop:
            emit(f"__m256d kern_z_cache[{wkernel_regs}];")
        kern_z_vars = [ f"kern_z_cache[{s}]" if not unroll_z_loop else f"kern_z_{s}"
                        for s in range(wkernel_regs) ]
        load_kernel("    ", "wkernel->data+sub_offset_z", kern_z_vars, wkernel_regs, unroll_z_loop)
    else:
        emit(f"__attribute__((aligned({avx_align}))) double kern_z_cache[{wkernel_size}];")
        copy_kernel("    ", "wkernel->data+sub_offset_z", "kern_z_cache", wkernel_regs)
else:
    kern_z_vars = [ f"kern_z_{s}" for s in range(kernel_regs_dup) ]
    load_kernel_dup("    ", "wkernel->data+sub_offset_z", kern_z_vars, wkernel_regs,
                    declare=True, shift=wphase)

# Load first Y kernel
if kernel_y_load:
    if not unroll_y_loop:
        emit(f"    __m256d kern_y_cache[{kernel_regs}];")
    kern_y_vars = [ f"kern_y_cache[{s}]" if not unroll_y_loop else f"kern_y_{s}"
                    for s in range(kernel_regs) ]
    load_kernel("    ", "kernel->data+sub_offset_y", kern_y_vars, kernel_regs, unroll_y_loop)

else:
    copy_kernel("    ", "kernel->data+sub_offset_y", "kern_y_cache")
    emit(f"__attribute__((aligned({avx_align}))) double kern_y_cache[{kernel_size}];")
if prepermute_y:
    if prepermute_y == 0.5:
        kern_y_pp_vars = [ f"kern_y_{s//2}_pp{s%2}" for s in range(kernel_size // 2) ]
        halfpermute_kernel("    ", kern_y_vars, kern_y_pp_vars, kernel_size // 4, True)
    else:
        kern_y_pp_vars = [ f"kern_y_{s//4}_pp{s%4}" for s in range(kernel_size) ]
        prepermute_kernel("    ", kern_y_vars, kern_y_pp_vars, kernel_size, True)

# Load first X kernel
if not interleave_wplanes:
    kern_x_vars = [ f"kern_x_{s}" for s in range(kernel_regs_dup) ]
    load_kernel_dup("    ", "kernel->data+sub_offset_x", kern_x_vars, kernel_regs, declare=True)
else:
    kern_x_vars = [ f"kern_x_{s}" for s in range(kernel_regs) ]
    load_kernel("    ", "kernel->data+sub_offset_x", kern_x_vars, kernel_regs, True)

    if prepermute_x == 0.5:
        kern_x_pp_vars = [ f"kern_x_{s//2}_pp{s%2}" for s in range(kernel_size // 2) ]
        halfpermute_kernel("    ", kern_x_vars, kern_x_pp_vars, kernel_size // 4, True)
    elif prepermute_x:
        kern_x_pp_vars = [ f"kern_x_{s//4}_pp{s%4}" for s in range(kernel_size) ]
        prepermute_kernel("    ", kern_x_vars, kern_x_pp_vars, kernel_size, True)


# Now generate loop over visibilities
emit(f"""
    for (; ch < end_ch; ch++, u += du/grid_du, v += dv/grid_dv, w += dw/grid_dw, pv++) {{

        int grid_offset = next_grid_offset;
        int plane_offset = next_plane_offset;
        if (ch + 1 < end_ch) {{""")

calc_frac_coord('            ', 'u+du/grid_du', 'v+dv/grid_dv', 'w+dw/grid_dw')

emit("""        }
        double *next_kernel_x = kernel->data + sub_offset_x;
        double *next_kernel_y = kernel->data + sub_offset_y;
        double *next_kernel_z = wkernel->data + sub_offset_z;""")

# Prefetch kernel for next visibility
def prefetch_kernel(ind, ptr):
    for s in range(kernel_size * double_size // cache_line_size):
        off = s * cache_line_size // double_size
        emit(f"{ind}_mm_prefetch({ptr}+{off}, _MM_HINT_T0);")
prefetch_kernel("        ", "next_kernel_x")
prefetch_kernel("        ", "next_kernel_y")
prefetch_kernel("        ", "next_kernel_z")
emit("")

def sum_row(ind, grid_ptr, kern_vars, suff=""):

    # Determine how many terms we need to sum, and how many parallel
    # sums we are going to keep
    terms = kernel_size * double_complex_size // register_size
    sum_vars = [ f"sum{suff}{s}" for s in range(min(terms, parallel_sums)) ]
    flops = 0
    for s, (svar, kvar) in enumerate(zip(sum_vars, kern_vars)):
        off = s * register_size // double_size
        emit(f"{ind}__m256d {svar} = _mm256_mul_pd({kvar}, _mm256_loadu_pd({grid_ptr}+{off}));")
        if debug_output:
            emit_print_cpx_reg(ind, f"_mm256_loadu_pd({grid_ptr}+{off})")
            emit(f'{ind}printf("*");')
            emit_print_cpx_reg(ind, kvar)
        flops += register_size // double_size
    # Cover remaining row
    for o in range(parallel_sums, terms, parallel_sums):
        for s, (svar, kvar) in enumerate(zip(sum_vars, kern_vars[o:])):
            off = (s+o) * register_size // double_size
            emit(f"{ind}{svar} = _mm256_fmadd_pd({kvar}, _mm256_loadu_pd({grid_ptr}+{off}), {svar});");
            if debug_output:
                emit_print_cpx_reg(ind, f"_mm256_loadu_pd({grid_ptr}+{off})")
                emit(f'{ind}printf("*");')
                emit_print_cpx_reg(ind, kvar)
            flops += 2 * register_size // double_size
    if debug_output:
        emit(f'{ind}puts("");')
    # Generate sum term to return
    flops += (len(sum_vars) - 1) * register_size // double_size
    return "+".join(sum_vars), flops

def sum_row_interleaved(ind, grid_ptr, kern_z_vars, kern_x_vars, suff=""):

    # Loop over x
    dpr = register_size // double_size
    flops = 0
    out = f"out{suff}"
    for x in range(kernel_size):
        off = x * wkernel_size * double_complex_size // double_size

        # Loop over z
        svar = f"sum{suff}{x}"
        emit(f"{ind}__m256d {svar} = _mm256_mul_pd({kern_z_vars[0]}, _mm256_load_pd({grid_ptr}+{off}));")
        if debug_output:
            emit(f'{ind}printf("x{x} z0:");')
            emit_print_cpx_reg(ind, f"_mm256_load_pd({grid_ptr}+{off})")
            emit(f'{ind}printf("*");')
            emit_print_cpx_reg(ind, kern_z_vars[0])
        flops += dpr
        for z in range(1,wkernel_regs_dup):
            zoff = off + z * register_size // double_size
            emit(f"{ind}{svar} = _mm256_fmadd_pd({kern_z_vars[z]}, _mm256_load_pd({grid_ptr}+{zoff}), {svar});")
            flops += 2 * dpr
            if debug_output:
                emit(f'{ind}printf("x{x} z{z}:");')
                emit_print_cpx_reg(ind, f"_mm256_load_pd({grid_ptr}+{zoff})")
                emit(f'{ind}printf("*");')
                emit_print_cpx_reg(ind, kern_z_vars[z])
        if debug_output:
            emit(f'{ind}puts("");')

        # Get x kernel
        kern_x_expr = kern_x_vars[x // dpr]
        if not prepermute_x:
            permut = make_permute(x%dpr,x%dpr,x%dpr,x%dpr)
            mult_expr = f"_mm256_permute4x64_pd({kern_x_expr},{permut})"
        elif prepermute_x == 0.5:
            mult_expr = f"_mm256_permute_pd({kern_x_expr}_pp{(x%dpr)//2}, {(x%2)*15})"
        else:
            mult_expr = f"{kern_x_expr}_pp{x%dpr}"

        # Sum / set
        if x:
            emit(f"{ind}{out} = _mm256_fmadd_pd({mult_expr}, {svar}, {out});")
            flops += 2 * dpr
        else:
            emit(f"{ind}__m256d {out} = _mm256_mul_pd({mult_expr}, {svar});")
            flops += dpr
        if debug_output:
            emit(f'{ind}printf("*");')
            emit_print_cpx_reg(ind, mult_expr)
            emit(f'{ind}puts("(x)");')
            emit(f'{ind}printf("=");')
            emit_print_cpx_reg(ind, out)
            emit(f'{ind}puts("(x)");')

    return out, flops

kernel_y_reg_cache = False
def sum_row_group(ind, grid_ptr, y_expr, out='visy', suff="", inter_z_vars=None):

    if kernel_y_load:
        if unroll_y_loop:
            kern_y_expr = kern_y_vars[int(y_expr)*double_size//register_size]
        else:
            kern_y_expr = f"kern_y_cache[{y_expr}/{register_size//double_size}]"
    else:
        kern_y_expr = f"_mm256_load_pd(kern_y_cache + {y_expr})"

    if kernel_y_reg_cache:
        emit(f"{ind}__m256d kerny{suff} = {kern_y_expr};")
        kern_y_expr = f"kerny{suff}"

    flops_sum = 0
    for u in range(register_size // double_size):
        # Get pointer to grid row
        emit(f"{ind}double *pgrid{suff}{u} = (double *)({grid_ptr} + ({y_expr}+{u})*grid_v_stride);")
        # Permute the y-kernel appropriately. We do this before loading
        # the grid line, as the permutation has quite a bit of latency
        if prepermute_y == 0.5:
            mult_expr = f"_mm256_permute_pd({kern_y_expr}_pp{u//2}, {(u%2)*15})"
        elif prepermute_y:
            mult_expr = f"{kern_y_expr}_pp{u}"
        else:
            permut = make_permute(u,u,u,u)
            mult_expr = f"ky{suff}{u}"
            emit(f"{ind}__m256d {mult_expr} = _mm256_permute4x64_pd({kern_y_expr},{permut});")
        # Generate sum over row, multipy/add into visibility accumulator
        if debug_output:
            emit(f'{ind}printf(" y{u} -\\n");')
        if inter_z_vars is None:
            sum_term, flops = sum_row(ind, f"pgrid{suff}{u}", kern_x_vars, suff=suff+str(u))
        else:
            sum_term, flops = sum_row_interleaved(ind, f"pgrid{suff}{u}", inter_z_vars, kern_x_vars,
                                                  suff=suff+str(u))
        emit(f"{ind}{out} = _mm256_fmadd_pd({sum_term}, {mult_expr}, {out});")
        if debug_output:
            emit(f'{ind}printf("*");')
            emit_print_cpx_reg(ind, mult_expr)
            emit(f'{ind}puts(" (y)");')
            emit(f'{ind}printf("=");')
            emit_print_cpx_reg(ind,  out )
            emit(f'{ind}puts("(y)");')
        flops_sum += flops + 2 * register_size // double_size

    return flops_sum

def sum_plane(ind, grid_expr, out, suff='', inter_z_vars=None):
    # Loop over rows in a plane. This can be done either as a loop
    # over groups, or unrolled entirely
    if unroll_y_loop:
        flops_sum = 0
        emit(f"{ind}__m256d {out} = _mm256_setzero_pd();")
        for y in range(0, kernel_size, register_size // double_size):
            flops_sum += sum_row_group(ind, grid_expr, str(y), out, suff+str(y), inter_z_vars)
        return flops_sum
    else:
        # Generate inner loop, unrolled so we can load as many values of the
        # y-direction kernel at once as possible
        emit(f"""
            __m256d {out} = _mm256_setzero_pd();
            int y;
            for (y = 0; y < {kernel_size}; y += {register_size // double_size}) {{""")

        flops_sum = sum_row_group(f"{ind}    ", grid_expr, "y", out, suff, inter_z_vars)

        emit(f"{ind}}}")
        return flops_sum * register_size // double_size

def sum_plane_group(ind, z_expr, out, loops=register_size // double_size, suff=''):

    if kernel_z_load:
        if unroll_z_loop:
            kern_z_expr = kern_z_vars[int(z_expr)*double_size//register_size]
        else:
            kern_z_expr = f"kern_z_cache[{z_expr}/{register_size//double_size}]"
    else:
        kern_z_expr = f"_mm256_load_pd(kern_z_cache + {z_expr})"

    flops_sum = 0
    for u in range(loops):
        # Get pointer to plane
        emit(f"{ind}complex double *pplane{suff}{u} = grid + (({z_expr}+{u}+plane_offset+grid_w_roll) % w_size) * grid_w_stride+grid_offset;")
        if debug_output:
            emit(f'{ind}printf("z{u} -\\n");')
        # Permute the y-kernel appropriately. We do this before loading
        # the grid line, as the permutation has quite a bit of latency
        permut = make_permute(u,u,u,u)
        emit(f"{ind}__m256d kz{suff}{u} = _mm256_permute4x64_pd({kern_z_expr},{permut});")
        # Generate sum over row, multipy/add into visibility accumulator
        flops = sum_plane(ind, f"pplane{suff}{u}", f'visy{suff}{u}', suff=suff+str(u))
        emit(f"{ind}{out} = _mm256_fmadd_pd(visy{suff}{u}, kz{suff}{u}, {out});")
        if debug_output:
            emit(f'{ind}printf("*");')
            emit_print_cpx_reg(ind, f'kz{suff}{u}')
            emit(f'{ind}puts(" (z)");')
            emit(f'{ind}printf("=");')
            emit_print_cpx_reg(ind, out)
            emit(f'{ind}puts("(z)");')
        flops_sum += flops + 2 * register_size // double_size

    return flops_sum


if interleave_wplanes:

    # Loop over rows in a plane. This can be done either as a loop
    # over groups, or unrolled entirely
    flops_sum = sum_plane('        ', f'uvgrids[{wphase}]+grid_offset', 'vis', '', kern_z_vars)

else:
    if not unroll_z_loop:
        emit(f"""
        __m256d vis = _mm256_setzero_pd();
        int z;
        for (z = 0; z < {wkernel_size}; z += {register_size // double_size}) {{
""")

        flops_sum = wkernel_regs * sum_plane_group(
            '            ', 'z', 'vis')

        emit(f"""
        }}""")
    else:
        emit(f"""
        __m256d vis = _mm256_setzero_pd();
""")

    flops_sum = 0
    for z in range(wkernel_regs):
        flops_sum += sum_plane_group('        ', int(z*register_size//double_size), 'vis',
                                     loops=min(wkernel_size - z * register_size // double_size,
                                               register_size // double_size),
                                     suff=str(z))

#     emit(f"""
#     __m256d vis = _mm256_setzero_pd();
#     int z;
#     for (z = 0; z < {wkernel_size}; z++) {{
#         complex double *pplane = uvgrids[z]+grid_offset;
# """)

#     flops_sum = wkernel_size * sum_plane('pplane', 'visy')

#     emit(f"""
#         double _kz = kern_z_cache[z/{register_size // double_size}][z%{register_size // double_size}];
#         __m256d kz = _mm256_set_pd(_kz, _kz, _kz, _kz);
#         vis = _mm256_fmadd_pd(visy, kz, vis);
#     }}""")


# Copy next kernels
if not interleave_wplanes:
    load_kernel_dup("        ", "next_kernel_x", kern_x_vars, kernel_regs)
    if kernel_z_load:
        load_kernel("        ", "next_kernel_z", kern_z_vars, wkernel_regs)
    else:
        copy_kernel("        ", "next_kernel_z", "kern_z_cache", wkernel_regs)
else:
    load_kernel("        ", "next_kernel_x", kern_x_vars, kernel_regs)
    if prepermute_x == 0.5:
        kern_x_pp_vars = [ f"kern_x_{s//2}_pp{s%2}" for s in range(kernel_size // 2) ]
        halfpermute_kernel("", kern_x_vars, kern_x_pp_vars, kernel_size // 4)
    elif prepermute_x:
        kern_x_pp_vars = [ f"kern_x_{s//4}_pp{s%4}" for s in range(kernel_size) ]
        prepermute_kernel("", kern_x_vars, kern_x_pp_vars, kernel_size)
    load_kernel_dup("        ", "next_kernel_z", kern_z_vars, wkernel_regs, shift=wphase)

if kernel_y_load:
    load_kernel("        ", "next_kernel_y", kern_y_vars, kernel_regs)
else:
    copy_kernel("        ", "next_kernel_y", "kern_y_cache", kernel_regs)
if prepermute_y == 0.5:
    halfpermute_kernel("        ", kern_y_vars, kern_y_pp_vars, kernel_size // 4)
elif prepermute_y:
    prepermute_kernel("        ", kern_y_vars, kern_y_pp_vars, kernel_size)


# Store visibility and conjugate. We don't count this floating point
# operations, it's overhead strictly speaking.
emit(f"""        __m128d vis_out = _mm256_extractf128_pd(vis, 0) + _mm256_extractf128_pd(vis, 1);""")

if debug_output:
    emit(f'        printf("->");')
    emit_print_cpx('        ', 'vis_out', 0)
    emit(f'        puts("");')

emit(f"""
        _mm_store_pd((double *)pv, _mm_xor_pd(vis_out, conj_mask));
    }}
    //*flops += {flops_sum} * (i1 - i0);
}} else """)

print(f"{kernel_size}x{wkernel_size}: {flops_sum} flops/vis")
