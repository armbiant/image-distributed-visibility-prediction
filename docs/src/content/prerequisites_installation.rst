Prerequisites and Installation
==============================

The following prerequisites must be installed:

* gcc
* cmake
* git
* `git lfs <https://git-lfs.github.com/>`_
* python3
* HDF5 (doesn't need to have parallel support)
* OpenMP
* MPI (with threading support)
* FFTW3.
* Optional: Singularity (only required if building/running
  containerised images of the code).

On the MacOS, ``homebrew`` can be used to install all the listed
dependencies.

The compilation process will use ``mpicc``, and it has been tested
with both GCC and ICC. It has also been tested on both Linux and MacOS.

To set up the repository, get data files, and compile, run the
following steps:

::

   git clone git@gitlab.com:ska-telescope/sdp/ska-sdp-exec-iotest.git
   cd ska-sdp-exec-iotest
   git lfs pull origin
   mkdir build
   cd build/
   cmake ../
   make -j$(nproc)

Singularity image can be pulled from GitLab registry using ``oras`` library.

::

   singularity pull iotest.sif oras://registry.gitlab.com/ska-telescope/sdp/ska-sdp-exec-iotest/iotest:latest

To build the image with Singularity:

::

   singularity build iotest.sif ska-sdp-exec-iotest.def
