.. skeleton documentation master file, created by
   sphinx-quickstart on Thu May 17 15:17:35 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. HOME SECTION ==================================================

.. Hidden toctree to manage the sidebar navigation.

.. toctree::
  :maxdepth: 2
  :caption: Home
  :hidden:

SDP Exec - Imaging IO Test
==========================

The following sections provide the context and design aspects of the `Imaging I/O Test <https://gitlab.com/ska-telescope/sdp/ska-sdp-exec-iotest>`_ prototype that is built to explore the capability of hardware and software to deal with the types of I/O loads that the SDP will have to support for
full-scale operation on SKA1 (and beyond).

.. toctree::
  :maxdepth: 1


  ../content/background
  ../content/design

This part deals with practical aspects of the benchmark on how to install and run on HPC machines.

.. toctree::
  :maxdepth: 1

  ../content/prerequisites_installation
  ../content/parameterisation
  ../content/configuration
  ../content/mpi_settings
  ../content/compute_resources
  ../content/issues


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
