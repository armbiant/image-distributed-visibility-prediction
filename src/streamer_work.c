#include "grid.h"
#include "config.h"
#include "streamer.h"

#include <hdf5.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <complex.h>
#include <string.h>
#include <omp.h>
#include <float.h>
#include <pthread.h>

#include <sys/mman.h>
#include <sys/wait.h>

/**
 * \brief Check the error in the streamed visibility data after applying degridding coordinate adjustments.
 */
static void streamer_degrid_check_visibility(struct streamer *streamer,
                                             double complex vis_out,
                                             bool conjugate,
                                             double check_u, double check_v, double check_w,
                                             double mid_w,
                                             double min_u, double max_u,
                                             double min_v, double max_v,
                                             double min_w, double max_w,
                                             uint64_t *square_error_samples,
                                             double *square_error_sum, double *worst_err)
{
    const int source_count = streamer->work_cfg->source_count;
    const int image_size = streamer->work_cfg->recombine.image_size;

    // Check that we actually generated a visibility here,
    // negate if necessary
    complex double vis = 0;
    if (check_u >= min_u && check_u < max_u &&
        check_v >= min_v && check_v < max_v &&
        check_w >= min_w && check_w < max_w) {

        // Apply (effective) coordinate adjustments degridding code would have done
        if (streamer->work_cfg->vis_round_to_wplane)
            check_w = mid_w;
        if (conjugate) { check_u *= -1; check_v *= -1; check_w *= -1; }

        // Undo uvw shear (see vis_spec_to_bl_data)
        const double shear_u = streamer->work_cfg->shear_u;
        const double shear_v = streamer->work_cfg->shear_v;
        double orig_w = check_w + shear_u * check_u + shear_v * check_v;

        // Generate visibility
        int i;
        const double *source_lmn = streamer->work_cfg->source_lmn;
        for (i = 0; i < source_count; i++) {
            // Calculate phase, sum visibility
            double l = source_lmn[i*3+0], m = source_lmn[i*3+1], n = source_lmn[i*3+2];
            double ph = check_u * l + check_v * m + check_w * n;
            vis += cos(2*M_PI*ph) + 1.j * sin(2*M_PI*ph);
            // Undo the shear to (l,m) to make sure we would arrive at
            // the same result without it (which is rather trivial to
            // see algebraically) as well as that the unsheared points
            // actually originated on the sky sphere (which is the
            // hard bit).
            double orig_l = l - shear_u * n, orig_m = m - shear_v * n;
            assert(fabs(sqrt(orig_l*orig_l + orig_m*orig_m + (n+1)*(n+1)) - 1) < 1e-13);
            double ph2 = check_u * orig_l + check_v * orig_m + orig_w * n;
            assert(fabs(ph - ph2) < 1e-10);

        }
        vis /= (double)image_size * image_size;

    }

    // Check error
    (*square_error_samples) += 1;
    double err = cabs(vis_out - vis);
    if (err / streamer->work_cfg->source_energy > streamer->work_cfg->vis_check_error) {
        printf("WARNING: uvw %.8g/%.8g/%.8g: %g%+gj != %g%+gj (err %g)\n",
               check_u, check_v, check_w, creal(vis_out), cimag(vis_out), creal(vis), cimag(vis),
               err / streamer->work_cfg->source_energy);
        printf("range: u: %g:%g v: %g:%g w: %g:%g\n", min_u, max_u, min_v, max_v, min_w, max_w);
    }
    (*worst_err) = fmax(err, *worst_err);
    (*square_error_sum) += err * err;

}

uint64_t streamer_degrid_worker(struct streamer *streamer,
                                struct bl_data *bl_data,
                                int SG_stride, double complex **subgrids,
                                double mid_u, double mid_v, double mid_w,
                                int iu, int iv, int iw,
                                int off_u, int off_v, int off_w,
                                bool conjugate,
                                int it0, int it1, int if0, int if1,
                                double min_u, double max_u,
                                double min_v, double max_v,
                                double min_w, double max_w,
                                double complex *vis_data)
{
    struct vis_spec *const spec = &streamer->work_cfg->spec;
    const double theta = streamer->work_cfg->theta;
    const int subgrid_size = streamer->work_cfg->recombine.xM_size;
    const double wstep = streamer->work_cfg->wstep;

    // Calculate l/m positions of sources in case we are meant to
    // check them
    const int source_count = streamer->work_cfg->source_count;

    // Initialise counter to random value so we check random visibilities
    int source_checks = streamer->work_cfg->vis_checks;
    int check_counter = 0;
    if (source_checks > 0 && source_count > 0) {
        check_counter = rand() % source_checks;
    } else {
        source_checks = 0;
    }

    // Bounds in (sub)grid coordinates. We only care about the
    // oversampling offset for the w-direction as we should know it to
    // be in ]-1+1/wkern_ov/2,1/wkern_ov/2]. However, to be compatible
    // with _frac_coord assumptions we must make it positive.
    const double grid_min_u = subgrid_size/2 + theta*(min_u-mid_u);
    const double grid_min_v = subgrid_size/2 + theta*(min_v-mid_v);
    const double grid_min_w = 1 + (min_w-mid_w) / wstep;
    const double grid_max_u = subgrid_size/2 + theta*(max_u-mid_u);
    const double grid_max_v = subgrid_size/2 + theta*(max_v-mid_v);
    const double grid_max_w = 1 + (max_w-mid_w) / wstep;
    assert(grid_min_u > 0); assert(grid_min_v > 0); assert(grid_min_w > 0);

    // Determine strides
    int x_stride = 1, y_stride = SG_stride;
    if (streamer->work_cfg->vis_interleave_wplanes) {
        x_stride *= streamer->work_cfg->w_gridder.size;
        y_stride *= streamer->work_cfg->w_gridder.size;
    }

    // Determine gridding coordinates (TODO: These should be
    // pre-calculated)
    uint64_t flops = 0;
    int time;
    double *uvw_ld = alloca(6 * sizeof(double) * (it1 - it0));
    for (time = it0; time < it1; time++) {

        double u = uvw_lambda(bl_data, time, 0, 0);
        double v = uvw_lambda(bl_data, time, 0, 1);
        double w = uvw_lambda(bl_data, time, 0, 2);
        double du = uvw_lambda(bl_data, time, 1, 0) - u;
        double dv = uvw_lambda(bl_data, time, 1, 1) - v;
        double dw = uvw_lambda(bl_data, time, 1, 2) - w;
        if (conjugate) {
            u *= -1; du *= -1; v *= -1; dv *= -1; w *= -1; dw *= -1;
        }
        uvw_ld[6*(time-it0)+0] = u;
        uvw_ld[6*(time-it0)+1] = v;
        uvw_ld[6*(time-it0)+2] = w;
        uvw_ld[6*(time-it0)+3] = du;
        uvw_ld[6*(time-it0)+4] = dv;
        uvw_ld[6*(time-it0)+5] = dw;

    }

    // Clamp channels
    int *channels = alloca(2 * sizeof(int) * (it1 - it0));
    int nvis =  clamp_channels(it0, it1-it0, if0, if1-if0,
                               min_u, max_u,
                               min_v, max_v,
                               min_w, max_w,
                               uvw_ld, channels);
    if (!nvis) {
        return 0;
    }

    // FLOP calculation (minimum)
    // * 4 operations per grid point (multiply by inner kernel + sum)
    flops += nvis * 4 *
        streamer->work_cfg->gridder.size *
        streamer->work_cfg->gridder.size *
        streamer->work_cfg->w_gridder.size;
    // * 2 flops per grid point for multiplication by gridding kernel
    //   (sum already included in above)
    flops += nvis * 2 *
        streamer->work_cfg->gridder.size *
        streamer->work_cfg->w_gridder.size;
    // * 2 flops per grid point for multiplication by wgridding kernel
    //   (sum already included in above)
    flops += nvis * 2 *
        streamer->work_cfg->w_gridder.size;

    // Find subgrid with lowest address... Which we will assume is the
    // start of a block with all the remaining subgrids, just permuted
    // in a "rolled" fasion. TODO: Pass it like this from the start...
    int w_roll;
    const int wkern_size = streamer->work_cfg->w_gridder.size;
    for (w_roll = 0; w_roll < wkern_size; w_roll++)
        if (subgrids[(w_roll+wkern_size-1) % wkern_size] > subgrids[w_roll])
            break;
    double complex *subgrid0 = subgrids[w_roll];
    int z_stride = subgrids[(w_roll+1) % wkern_size] - subgrids[w_roll];

    // Degrid visibilities
    struct gridder_plan plan =
        {
         1 / theta,
         1 / theta,
         wstep,
         streamer->work_cfg->gridder,
         streamer->work_cfg->w_gridder
        };
    degrid(off_u-subgrid_size/2, subgrid_size,
           off_v-subgrid_size/2, subgrid_size,
           off_w-wkern_size/2, wkern_size,
           it0, it1-it0, if0, if1-if0,
           &plan, conjugate, uvw_ld, channels,
           subgrid0, x_stride, y_stride, z_stride, wkern_size-w_roll,
           vis_data, 1, spec->freq_chunk);

    // Check against DFT (one per row, maximum)
    uint64_t square_error_samples = 0;
    double square_error_sum = 0, worst_err = 0;
    for (time = it0; time < it1; time++) {
        if (source_checks > 0) {
            if (check_counter >= if1 - if0) {
                check_counter -= if1 - if0;

            } else {

                double complex vis_out = vis_data[(time-it0) * spec->freq_chunk + check_counter];
                double check_u = uvw_ld[6*(time-it0)+0] + uvw_ld[6*(time-it0)+3] * (if0 + check_counter);
                double check_v = uvw_ld[6*(time-it0)+1] + uvw_ld[6*(time-it0)+4] * (if0 + check_counter);
                double check_w = uvw_ld[6*(time-it0)+2] + uvw_ld[6*(time-it0)+5] * (if0 + check_counter);

                streamer_degrid_check_visibility(streamer, vis_out, conjugate, check_u, check_v, check_w,
                                                 mid_w, min_u, max_u, min_v, max_v, min_w, max_w,
                                                 &square_error_samples, &square_error_sum,
                                                 &worst_err);
                check_counter = source_checks;
            }
        }
    }

    // Add to statistics
    #pragma omp atomic
        streamer->vis_error_samples += square_error_samples;
    #pragma omp atomic
        streamer->vis_error_sum += square_error_sum;
    if (worst_err > streamer->vis_worst_error) {
        // Likely happens rarely enough that a critical section won't be a problem
        #pragma omp critical
           streamer->vis_worst_error = fmax(streamer->vis_worst_error, worst_err);
    }
    #pragma omp atomic
        streamer->degrid_flops += flops;
    #pragma omp atomic
        streamer->degrid_vis_count += nvis;
    #pragma omp atomic
        streamer->produced_chunks += 1;

    return flops;
}

/**
 * \brief Degrid a chunk of streamed visibility data.
 * \return True if the operation succeeds in degridding the chunk of visibility data, false otherwise.
 */
bool streamer_degrid_chunk(struct streamer *streamer,
                           struct subgrid_work *work,
                           struct subgrid_work_bl *bl,
                           int tchunk, int fchunk,
                           int slot,
                           int SG_stride, double complex **subgrids,
                           int wplane)
{
    struct vis_spec *const spec = &streamer->work_cfg->spec;
    struct recombine2d_config *const cfg = &streamer->work_cfg->recombine;
    const double theta = streamer->work_cfg->theta;
    const double wstep = streamer->work_cfg->wstep;
    const int sg_step = streamer->work_cfg->sg_step;
    const int sg_step_w = streamer->work_cfg->sg_step_w;

    double start = get_time_ns();

    // Calculate subgrid boundaries. TODO: All of this duplicates
    // logic that also appears in config.c (bin_baseline). This is
    // brittle, should get refactored at some point!
    double sg_mid_u = work->subgrid_off_u / theta;
    double sg_mid_v = work->subgrid_off_v / theta;
    double sg_mid_w = (work->subgrid_off_w + wplane) * wstep;
    double sg_min_u = (work->subgrid_off_u - sg_step / 2) / theta;
    double sg_min_v = (work->subgrid_off_v - sg_step / 2) / theta;
    double sg_min_w = (work->subgrid_off_w - sg_step_w / 2) * wstep;
    double sg_max_u = (work->subgrid_off_u + sg_step / 2) / theta;
    double sg_max_v = (work->subgrid_off_v + sg_step / 2) / theta;
    double sg_max_w = (work->subgrid_off_w + sg_step_w / 2) * wstep;
    if (sg_min_v > cfg->image_size / theta / 2) {
        sg_min_v -= cfg->image_size / theta / 2;
        sg_max_v -= cfg->image_size / theta / 2;
    }

    // Calculate limits of w-plane, i.e. visibilities we can degrid
    // from the set of w-planes we are currently working on (which is
    // just one w-plane worth, as we are passed exactly as many
    // w-planes as the w-kernel is large). Note that due to limited
    // oversampling precision there is some "overlap" at the edges of
    // w-planes.
    const int wkern_over = streamer->work_cfg->w_gridder.oversampling;
    const double wp_min_w = (work->subgrid_off_w + wplane - 1) * wstep + wstep/2/wkern_over;
    const double wp_max_w = (work->subgrid_off_w + wplane) * wstep + wstep/2/wkern_over;
    sg_min_w = fmax(sg_min_w, wp_min_w);
    sg_max_w = fmin(sg_max_w, wp_max_w);

    // Determine chunk size
    int it0 = tchunk * spec->time_chunk,
        it1 = (tchunk+1) * spec->time_chunk;
    if (it1 > spec->time_count) it1 = spec->time_count;
    int if0 = fchunk * spec->freq_chunk,
        if1 = (fchunk+1) * spec->freq_chunk;
    if (if1 > spec->freq_count) if1 = spec->freq_count;

    // Check whether time chunk fall into positive u. We use this
    // for deciding whether coordinates are going to get flipped
    // for the entire chunk. This is assuming that a chunk is
    // never big enough that we would overlap an extra subgrid
    // into the negative direction.
    int tstep_mid = (it0 + it1) / 2;
    bool positive_u = (bl->bl_data->uvw_m_2[tstep_mid * 3] - bl->bl_data->uvw_m_1[tstep_mid * 3]) >= 0;

    // Check for overlap between baseline chunk and subgrid
    double min_uvw[3], max_uvw[3];
    bl_bounding_box(bl->bl_data, !positive_u, it0, it1-1, if0, if1-1,
                    min_uvw, max_uvw);
    // double overlap =
    //    -fmax((fmax(min_uvw[0], sg_min_u) - fmin(max_uvw[0], sg_max_u)),
    //          (fmax(min_uvw[1], sg_min_v) - fmin(max_uvw[1], sg_max_v)));
    // if (bl->a1 == 0 && bl->a2 == 4) {
    //     printf("%d-%d it=%d-%d if=%d-%d "
    //            "u%g-%g [%g,%g] v%g-%g [%g,%g] w%g-%g [%g,%g] "
    //            "min_w=%g max_w=%g overlap=%g\n",
    //            bl->a1, bl->a2,
    //            it0,it1,if0,if1,
    //            min_uvw[0], max_uvw[0], sg_min_u, sg_max_u,
    //            min_uvw[1], max_uvw[1], sg_min_v, sg_max_v,
    //            min_uvw[2], max_uvw[2], sg_min_w, sg_max_w,
    //            bl->min_w, bl->max_w, overlap);
    // }
    if (!(min_uvw[0] < sg_max_u && max_uvw[0] > sg_min_u &&
          min_uvw[1] < sg_max_v && max_uvw[1] > sg_min_v &&
          min_uvw[2] < sg_max_w && max_uvw[2] > sg_min_w))
        return false;

    // Determine least busy writer
    int i, least_waiting = 2 * streamer->vis_queue_per_writer;
    struct streamer_writer *writer = streamer->writer;
    for (i = 0; i < streamer->writer_count; i++) {
        if (streamer->writer[i].to_write < least_waiting) {
            least_waiting = streamer->writer[i].to_write;
            writer = streamer->writer + i;
        }
    }

    // Acquire a slot
    struct streamer_chunk *chunk
        = writer_push_slot(writer, bl->bl_data, tchunk, fchunk);
    #pragma omp atomic
        streamer->wait_in_time += get_time_ns() - start;
    start = get_time_ns();

    // Do degridding
    const size_t chunk_vis_size = sizeof(double complex) * spec->freq_chunk * spec->time_chunk;
    uint64_t flops = streamer_degrid_worker(
        streamer, bl->bl_data, SG_stride, subgrids,
        sg_mid_u, sg_mid_v, sg_mid_w,
        work->iu, work->iv, work->iw,
        work->subgrid_off_u, work->subgrid_off_v, work->subgrid_off_w + wplane,
        !positive_u,
        it0, it1, if0, if1,
        sg_min_u, sg_max_u, sg_min_v, sg_max_v, sg_min_w, sg_max_w,
        chunk ? chunk->vis : alloca(chunk_vis_size));
    #pragma omp atomic
      streamer->degrid_time += get_time_ns() - start;

    if (chunk) {

        // No flops executed? Signal to writer that we can skip writing
        // this chunk (small optimisation)
        if (flops == 0) {
            chunk->tchunk = -2;
            chunk->fchunk = -2;
        }

        // Signal slot for output
#ifndef __APPLE__
        sem_post(&chunk->out_lock);
#else
        dispatch_semaphore_signal(chunk->out_lock);
#endif
    }

    return true;
}

/**
 * \brief Generate streamer subgrid from the discrete Fourier transform.
 */
void streamer_make_subgrid(struct streamer *streamer,
                           int dwplane, int xM_size, int SG_stride,
                           complex double *subgrid_image,
                           complex double *subgrid)
{
    double start_time = get_time_ns();
    double complex *const wtransfer = streamer->wtransfer;

    // Move to next w-plane
    int i;
    if (dwplane == 0) {
        // nothing to do
    } else if (dwplane == 1) {
        // special (common) case
        for (i = 0; i < xM_size * xM_size; i++) {
            subgrid_image[i] *= wtransfer[i];
        }
    } else {
        for (i = 0; i < xM_size * xM_size; i++) {
            subgrid_image[i] *= cipow(wtransfer[i], dwplane);
        }
    }

    // Generate subgrid
#ifndef USE_MKL
    fftw_execute_dft(streamer->subgrid_plan, subgrid_image, subgrid);
#else
    DftiComputeBackward(streamer->subgrid_plan, subgrid_image, subgrid);
#endif
    streamer->fft_time += get_time_ns() - start_time;
    streamer->fft_count++;

}

void streamer_task(struct streamer *streamer,
                   struct subgrid_work *work,
                   struct subgrid_work_bl *bl_start,
                   int slot,
                   double complex *subgrid_image_in)
{

    // Determine subgrid dimensions.
    const int xM_size = streamer->work_cfg->recombine.xM_size;
    const double wstep = streamer->work_cfg->wstep;

    // Determine how much space we need to cover along the w-axis
    struct subgrid_work_bl *bl;
    int i_bl; double min_w = bl_start->min_w, max_w = bl_start->max_w;
    for (bl = bl_start->next, i_bl = 1;
         bl && i_bl < streamer->work_cfg->vis_bls_per_task;
         bl = bl->next, i_bl++) {

        min_w = fmin(min_w, bl->min_w);
        max_w = fmax(max_w, bl->max_w);
    }

    // Determine w-planes we need to cover. Note that the w-kernel has
    // finite oversampling precision, which means that "n*wstep +
    // 1/wkern_over/2" will get rounded to "n*wstep" later.
    const int wkern_size = streamer->work_cfg->w_gridder.size;
    const int wkern_over = streamer->work_cfg->w_gridder.oversampling;
    int w_start = (int) ceil(min_w / wstep - 1./wkern_over/2) - work->subgrid_off_w;
    int w_end = (int) ceil(max_w / wstep - 1./wkern_over/2) - work->subgrid_off_w;

    // Allocate subgrids
    const int SG_stride = xM_size + streamer->work_cfg->vis_extra_grid_stride;
    const int SG2_size = sizeof(double complex) * SG_stride * xM_size;
    double complex *subgrid_image = fftw_malloc((1+wkern_size) * SG2_size);
    if (!subgrid_image) {
        fprintf(stderr, "ERROR: Failed to allocate %d bytes for subgrid transform!\n",
                (1+wkern_size) * SG2_size);
        exit(1);
    }
    double complex *subgrids = subgrid_image + SG_stride * xM_size;

    // Allocate subgrid pointers
    double complex **subgrid_ptrs =
        (double complex **)alloca(sizeof(double complex *) * wkern_size);
    int i;
    for (i = 0; i < wkern_size; i++) {
        if (streamer->work_cfg->vis_interleave_wplanes) {
            subgrid_ptrs[i] = subgrids + i;
        } else {
            subgrid_ptrs[i] = subgrids + i * SG_stride * xM_size;
        }
    }

    // Apply w-transfer pattern for start
    double complex *const wtransfer = streamer->wtransfer;
    if (subgrid_image_in) {
        int m = 1; assert(xM_size % 2 == 0);
        for (i = 0; i < xM_size * xM_size; i++) {
            // This multiplier is equivalent to fft_shift after FFT later on
            if (i % xM_size != 0) { m *= -1; }
            subgrid_image[i] = m * subgrid_image_in[i] *
                cipow(wtransfer[i], w_start - wkern_size/2);
        }

        // Fill first w-planes. This will cover the w-plane range of
        // [w_start-wkern_size/2, w_start-wkern_size/2+wkern_size-1]
        // (e.g. [w_start-2, w_start+1] for wkern_size=4)
        for (i = 0; i < wkern_size; i++) {
            streamer_make_subgrid(streamer, i == 0 ? 0 : 1, xM_size, SG_stride,
                                  subgrid_image, subgrid_ptrs[i]);
        }
    }

    int wplane;
    for (wplane = w_start; wplane <= w_end; wplane++) {

        if (subgrid_image_in && wplane > w_start) {
            // Overwrite subgrids for new w-planes in ring-buffer
            // fashion - i.e. current subgrid 0 becomes the new
            // subgrid +(wkern_size-1).
            double complex *new_subgrid = subgrid_ptrs[0];
            streamer_make_subgrid(streamer, 1, xM_size, SG_stride,
                                  subgrid_image, new_subgrid);
            // Now set new w-plane pointers
            for (i = 0; i < wkern_size - 1; i++) {
                subgrid_ptrs[i] = subgrid_ptrs[i+1];
            }
            subgrid_ptrs[wkern_size - 1] = new_subgrid;
        }

        const double wp_min_w = (work->subgrid_off_w + wplane - 1) * wstep + wstep/2/wkern_over;
        const double wp_max_w = (work->subgrid_off_w + wplane) * wstep + wstep/2/wkern_over;
        struct vis_spec *const spec = &streamer->work_cfg->spec;
        for (bl = bl_start, i_bl = 0;
             bl && i_bl < streamer->work_cfg->vis_bls_per_task;
             bl = bl->next, i_bl++) {

            // Make sure there's overlap with this w-plane
            if (bl->min_w > wp_max_w ||
                bl->max_w < wp_min_w)
                continue;

            // Go through time/frequency chunks
            int ntchunk = (bl_start->bl_data->time_count + spec->time_chunk - 1) / spec->time_chunk;
            int nfchunk = (bl_start->bl_data->freq_count + spec->freq_chunk - 1) / spec->freq_chunk;
            int nchunks = 0;
            int tchunk, fchunk;
            for (tchunk = 0; tchunk < ntchunk; tchunk++)
                for (fchunk = 0; fchunk < nfchunk; fchunk++)
                    if (streamer_degrid_chunk(streamer, work,
                                              bl, tchunk, fchunk,
                                              slot, SG_stride, subgrid_ptrs,
                                              wplane))
                        nchunks++;

            // Check that plan predicted the right number of chunks. This
            // is pretty important - if this fails this means that the
            // coordinate calculations are out of synch, which might mean
            // that we have failed to account for some visibilities in the
            // plan!
            //if (bl->chunks != nchunks)
            //    printf("WARNING: subgrid (%d/%d/%d) baseline (%d-%d) %d chunks planned, %d actual!\n",
            //           work->iu, work->iv, work->iw, bl->a1, bl->a2, bl->chunks, nchunks);

        }

    }

    // Done with this chunk
#pragma omp atomic
    streamer->subgrid_tasks--;
#pragma omp atomic
    streamer->subgrid_locks[slot]--;

    fftw_free(subgrid_image);
}

/**
 * \brief Perform checks on the subgrid data. Uses RMSE if we have sources to do the checks.
 */
static void streamer_checks(struct streamer *streamer,
                            struct subgrid_work *work,
                            complex double *subgrid_image,
                            double *p_err_sum, double *p_err_sum_wmax)
{
    struct recombine2d_config *const cfg = &streamer->work_cfg->recombine;

    // Perform Fourier transform
    const int xM_size = cfg->xM_size;
    const int SG_stride = xM_size + streamer->work_cfg->vis_extra_grid_stride;
    int SG_ext_size = sizeof(double complex) * SG_stride * xM_size;
    if (streamer->work_cfg->vis_interleave_wplanes) {
        SG_ext_size *= streamer->work_cfg->w_gridder.size;
    }
    complex double *subgrid_image_wmax = fftw_malloc(sizeof(complex double) * (cfg->SG_size + 2*SG_ext_size));
    complex double *subgrid = subgrid_image_wmax + cfg->SG_size;
    complex double *subgrid_wmax = subgrid + SG_ext_size;

    // Construct (image of) subgrid at maximum w-level of w-tower
    const int wplane_max = streamer->work_cfg->sg_step_w / 2 +
                           streamer->work_cfg->w_gridder.size / 2;
    const double complex *const wtransfer = streamer->wtransfer;
    int i, m = 1; assert(xM_size % 2 == 0);
    for (i = 0; i < xM_size * xM_size; i++) {
        // This multiplier is equivalent to fft_shift after FFT.
        if (i % xM_size != 0) { m *= -1; }
        subgrid_image_wmax[i] = m * subgrid_image[i] * cipow(wtransfer[i], wplane_max);
    }

#ifndef USE_MKL
    fftw_execute_dft(streamer->subgrid_plan, subgrid_image, subgrid);
    fftw_execute_dft(streamer->subgrid_plan, subgrid_image_wmax, subgrid_wmax);
#else
    DftiComputeBackward(streamer->subgrid_plan, subgrid_image, subgrid);
    DftiComputeBackward(streamer->subgrid_plan, subgrid_image_wmax, subgrid_wmax);
#endif
    streamer->fft_count += 2;

    // Interleaving w-planes? FFTs will be spaced out which makes zero
    // sense here, so somewhat awkwardly undo it. Might want special FFTs...
    if (streamer->work_cfg->vis_interleave_wplanes) {
        for (i = 0; i < xM_size * SG_stride; i++) {
            subgrid[i] = subgrid[streamer->work_cfg->w_gridder.size * i];
            subgrid_wmax[i] = subgrid_wmax[streamer->work_cfg->w_gridder.size * i];
        }
    }

    // Check accumulated result
    if (work->check_path && streamer->work_cfg->facet_workers > 0) {
        double complex *approx_ref = read_hdf5(cfg->SG_size, work->check_hdf5, work->check_path);
        double err_sum = 0; int y, x;
        for (y = 0; y < cfg->xM_size; y++) {
            for (x = 0; x < cfg->xM_size; x++) {
                double err = cabs(subgrid[y*SG_stride+x] - approx_ref[y*cfg->xM_size+x]);
                err_sum += err * err;
            }
        }
        free(approx_ref);
        double rmse = sqrt(err_sum / cfg->xM_size / cfg->xM_size);
        printf("%sSubgrid %d/%d RMSE %g\n",
               rmse > work->check_threshold ? "ERROR: " : "",
               work->iu, work->iv, rmse);

    }

    fft_shift(subgrid, cfg->xM_size, SG_stride);

    // Check some degridded example visibilities
    if (work->check_degrid_path && streamer->kern && streamer->work_cfg->facet_workers > 0) {
        int nvis = get_npoints_hdf5(work->check_hdf5, "%s/vis", work->check_degrid_path);
        double *uvw_sg = read_hdf5(3 * sizeof(double) * nvis, work->check_hdf5,
                                   "%s/uvw_subgrid", work->check_degrid_path);
        int vis_size = sizeof(double complex) * nvis;
        double complex *vis = read_hdf5(vis_size, work->check_hdf5,
                                        "%s/vis", work->check_degrid_path);

        struct bl_data bl;
        bl.antenna1 = bl.antenna2 = 0;
        bl.time_count = nvis;
        bl.freq_count = 1;
        double freq[] = { c }; // 1 m wavelength
        bl.freq = freq;
        bl.vis = (double complex *)calloc(1, vis_size);

        // Degrid and compare
        bl.uvw_m_1 = calloc(sizeof(double), 3 * nvis);
        bl.uvw_m_2 = uvw_sg;
        degrid_conv_bl(subgrid, cfg->xM_size, SG_stride, cfg->image_size, 0, 0,
                       -cfg->xM_size, cfg->xM_size, -cfg->xM_size, cfg->xM_size,
                       &bl, 0, nvis, 0, 1, streamer->kern);
        double err_sum = 0; int y;
        for (y = 0; y < nvis; y++) {
            double err = cabs(vis[y] - bl.vis[y]); err_sum += err*err;
        }
        double rmse = sqrt(err_sum / nvis);
        printf("%sSubgrid %d/%d degrid RMSE %g\n",
               rmse > work->check_degrid_threshold ? "ERROR: " : "",
               work->iu, work->iv, rmse);

        free(bl.vis); free(bl.uvw_m_1); free(bl.uvw_m_2);
    }

    // Check against DFT, if we are generating from sources
    *p_err_sum = *p_err_sum_wmax = -1;
    const int source_checks = streamer->work_cfg->grid_checks;
    const int source_count = streamer->work_cfg->source_count;
    if (source_count > 0 && source_checks > 0) {

        double err_sum = 0, err_sum_wmax = 0, worst_err = 0, worst_err_wmax = 0;
        int err_samples = 0;

        const double theta = streamer->work_cfg->theta;
        const double wstep = streamer->work_cfg->wstep;

        int iu, iv;
        int check_counter = rand() % source_checks;
        const int sg_step = streamer->work_cfg->sg_step;
        for (iv = -sg_step/2; iv < sg_step/2; iv++) {
            for (iu = check_counter-sg_step/2; iu < sg_step/2; iu+=source_checks) {

                double check_u = (work->subgrid_off_u+iu) / theta;
                double check_v = (work->subgrid_off_v+iv) / theta;
                double check_w = work->subgrid_off_w * wstep;

                // Generate visibility
                complex double vis = 0, vis_wmax = 0;
                int i;
                for (i = 0; i < source_count; i++) {
                    double ph =
                        check_u * streamer->work_cfg->source_lmn[i*3+0] +
                        check_v * streamer->work_cfg->source_lmn[i*3+1] +
                        check_w * streamer->work_cfg->source_lmn[i*3+2];
                    vis += 1/streamer->work_cfg->source_corr[i]
                        * (cos(2*M_PI*ph) + 1.j * sin(2*M_PI*ph));

                    // Calculate visibility at offset of one wstep
                    ph += wplane_max * wstep * streamer->work_cfg->source_lmn[i*3+2];
                    vis_wmax += 1/streamer->work_cfg->source_corr[i]
                        * (cos(2*M_PI*ph) + 1.j * sin(2*M_PI*ph));

                }
                vis /= (double)cfg->image_size * cfg->image_size;
                vis_wmax /= (double)cfg->image_size * cfg->image_size;

                // Check
                double complex vis_grid = subgrid[(iv+cfg->xM_size/2) * SG_stride + iu + cfg->xM_size/2];
                double complex vis_grid_wmax = subgrid_wmax[(iv+cfg->xM_size/2) * SG_stride + iu + cfg->xM_size/2];

                double err = cabs(vis_grid - vis);
                double err_wmax = cabs(vis_grid_wmax - vis_wmax);
                err_sum += err * err;
                err_sum_wmax += err_wmax * err_wmax;
                worst_err = fmax(worst_err, err);
                worst_err_wmax = fmax(worst_err_wmax, err_wmax);
                err_samples += 1;

            }
            check_counter = iu - sg_step/2;
        }

        #pragma omp atomic
             streamer->grid_error_samples += err_samples;
        #pragma omp atomic
             streamer->grid_error_sum += err_sum;
        #pragma omp atomic
             streamer->grid_wmax_error_sum += err_sum_wmax;
        if (worst_err > streamer->grid_worst_error) {
            #pragma omp critical
                 streamer->grid_worst_error = fmax(streamer->grid_worst_error, worst_err);
        }
        if (worst_err_wmax > streamer->grid_wmax_worst_error) {
            #pragma omp critical
                 streamer->grid_wmax_worst_error = fmax(streamer->grid_wmax_worst_error, worst_err_wmax);
        }

        if (err_samples > 0) {
            const double source_energy = streamer->work_cfg->source_energy;
            if (p_err_sum)
                *p_err_sum = sqrt(err_sum / err_samples) / source_energy;
            if (p_err_sum_wmax)
                *p_err_sum_wmax = sqrt(err_sum_wmax / err_samples) / source_energy;
        }

    }

    fftw_free(subgrid_image_wmax);
}

void streamer_work(struct streamer *streamer,
                   struct subgrid_work *work,
                   double complex *nmbf)
{

    struct recombine2d_config *const cfg = &streamer->work_cfg->recombine;
    struct facet_work *const facet_work = streamer->work_cfg->facet_work;

    const int facets = streamer->work_cfg->facet_workers * streamer->work_cfg->facet_max_work;
    const int nmbf_length = cfg->NMBF_NMBF_size / sizeof(double complex);

    // Find slot to write to
    int slot;
    double slot_start = get_time_ns();
    while(true) {
        for (slot = 0; slot < streamer->queue_length; slot++)
            if (streamer->subgrid_locks[slot] == 0)
                break;
        if (slot < streamer->queue_length)
            break;
        #pragma omp taskyield
        usleep(100);
    }
    streamer->slot_time += get_time_ns() - slot_start;

    double recombine_start = get_time_ns();

    // Compare with reference
    if (work->check_fct_path) {

        int i0 = work->iv, i1 = work->iu;
        int ifacet;
        for (ifacet = 0; ifacet < facets; ifacet++) {
            if (!facet_work[ifacet].set) continue;
            int j0 = facet_work[ifacet].im, j1 = facet_work[ifacet].il;
            double complex *ref = read_hdf5(cfg->NMBF_NMBF_size, work->check_hdf5,
                                            work->check_fct_path, j0, j1);
            int x; double err_sum = 0;
            for (x = 0; x < nmbf_length; x++) {
                double err = cabs(ref[x] - nmbf[nmbf_length*ifacet+x]); err_sum += err*err;
            }
            free(ref);
            double rmse = sqrt(err_sum / nmbf_length);
            if (!work->check_fct_threshold || rmse > work->check_fct_threshold) {
                printf("Subgrid %d/%d facet %d/%d checked: %g RMSE\n",
                       i0, i1, j0, j1, rmse);
            }
        }
    }

    // Accumulate contributions to this subgrid
    double complex *subgrid = subgrid_slot(streamer, slot);
    memset(subgrid, 0, cfg->SG_size);
    int ifacet;
    for (ifacet = 0; ifacet < facets; ifacet++)
        recombine2d_af0_af1(cfg, subgrid,
                            facet_work[ifacet].facet_off_m,
                            facet_work[ifacet].facet_off_l,
                            nmbf + nmbf_length*ifacet);
    streamer->recombine_time += get_time_ns() - recombine_start;

    // Perform checks on result
    double check_start = get_time_ns();
    double rmse, rmse_wmax; streamer_checks(streamer, work, subgrid, &rmse, &rmse_wmax);
    streamer->check_time += get_time_ns() - check_start;

    double task_start = get_time_ns();
    struct vis_spec *const spec = &streamer->work_cfg->spec;
    if (spec->time_count > 0 && streamer->kern) {

        // Loop through baselines
        struct subgrid_work_bl *bl;
        int i_bl = 0;
        for (bl = work->bls; bl; bl = bl->next, i_bl++) {
            if (i_bl % streamer->work_cfg->vis_bls_per_task != 0)
                continue;

            // We are spawning a task: Add lock to subgrid data to
            // make sure it doesn't get overwritten
            #pragma omp atomic
              streamer->subgrid_tasks++;
            #pragma omp atomic
              streamer->subgrid_locks[slot]++;

            // Start task. Make absolutely sure it sees *everything*
            // as private, as Intel's C compiler otherwise loves to
            // generate segfaulting code. OpenMP complains here that
            // having a "private" constant is unecessary (requiring
            // the copy), but I don't trust its judgement.
            struct subgrid_work *_work = work;
            #pragma omp task firstprivate(streamer, _work, bl, slot, subgrid)
                streamer_task(streamer, _work, bl, slot, subgrid);
        }

        if (rmse_wmax >= 0) {
            printf("Subgrid %d/%d/%d (%d baselines, rmse %.03g, @wmax: %.03g)\n",
                   work->iu, work->iv, work->iw, i_bl, rmse, rmse_wmax);
        } else if (rmse >= 0) {
            printf("Subgrid %d/%d/%d (%d baselines, rmse %.03g)\n",
                   work->iu, work->iv, work->iw, i_bl, rmse);
        } else {
            printf("Subgrid %d/%d/%d (%d baselines)\n",
                   work->iu, work->iv, work->iw, i_bl);
        }
        fflush(stdout);
        streamer->baselines_covered += i_bl;

    }
    streamer->task_start_time += get_time_ns() - task_start;
}
