#ifndef CONFIG_H
#define CONFIG_H

#include "recombine.h"
#include "grid.h"

#ifndef NO_MPI
#include <mpi.h>
#else
#define MPI_Request int
#define MPI_REQUEST_NULL 0
#endif

#ifndef __APPLE__
#include <semaphore.h>
#else
#include <dispatch/dispatch.h>
#define sem_t dispatch_semaphore_t
#endif

/**
 * \struct recombine_parset
 * \brief Parameter set for streaming Fourier transform
 */
struct recombine_parset {
    const char *name;
    double W, fov;
    int image_size;
    int Nx;
    int yB_size, yN_size, yP_size;
    int xA_size, xM_size, xMxN_yP_size;
};

/**
 * \struct vis_spec
 * \brief Specification of a visibility set.
 */
struct vis_spec
{
    double fov; // (true) field of view
    struct ant_config *cfg; // antennas
    double dec; // declination (radian)
    double time_start; int time_count; int time_chunk; double time_step; // hour angle (radian)
    double freq_start; int freq_count; int freq_chunk; double freq_step; // (Hz)
    // Cached hour angle / declination cosinus & sinus
    double *ha_sin, *ha_cos;
    double dec_sin, dec_cos;
};

inline static int spec_time_chunks(struct vis_spec *spec)
{
    return (spec->time_count + spec->time_chunk - 1) / spec->time_chunk;
}
inline static int spec_freq_chunks(struct vis_spec *spec)
{
    return (spec->freq_count + spec->freq_chunk - 1) / spec->freq_chunk;
}

void bl_bounding_box(struct bl_data *bl_data, bool negate,
                     int tstep0, int tstep1,
                     int fstep0, int fstep1,
                     double *uvw_l_min, double *uvw_l_max);

/**
 * \struct facet_work
 * \brief Work to do on a facet.
 */
struct facet_work {
    int il, im;
    int facet_off_l, facet_off_m;
    char *path, *hdf5; // random if not set
    bool set; // empty otherwise
};

/**
 * \struct subgrid_work_bl
 * \brief Work to do for a subgrid on a baseline.
 */
struct subgrid_work_bl {
    int a1, a2; // Baseline antennas
    int chunks; // Number of (time,frequency) chunks overlapping
    double min_w, max_w; // Minimum+maximum touched w-level (for sorting)
    struct bl_data *bl_data;
    struct subgrid_work_bl *next;
};

/**
 * \struct subgrid_work
 * \brief Work to do for a subgrid.
 */
struct subgrid_work {
    int iu, iv, iw; // Column/row/plane number. Used for grouping, so must be consistent across work items!
    int subgrid_off_u, subgrid_off_v, subgrid_off_w; // Midpoint offset in grid coordinates
    int nbl; // Baselines in this work bin
    int worker; // Worker this work is assigned to
    int tag; // MPI tag to use for exchanging subgrid data
    int assign_tag; // MPI tag to use for assigning the worker
    MPI_Request assign_request; // The MPI request that sets worker, if assignment delayed
    char *check_path, *check_fct_path,
         *check_degrid_path, *check_hdf5; // check data if set
    double check_threshold, check_fct_threshold,
           check_degrid_threshold; // at what discrepancy to fail
    struct subgrid_work_bl *bls; // Baselines
};

/**
 * \struct work_config
 * \brief Worker configuration data.
 */
struct work_config {

    // Fundamental dimensions (uvw grid / cubes)
    double theta; // size of (padded) image in radians (1/uvstep)
    double wstep; // distance of w-planes
    int sg_step, sg_step_w; // effective subgrid cube size (step length as above)
    struct vis_spec spec; // Visibility specification
    struct bl_data *bl_data; // Baseline data (e.g. UVWs)
    char *vis_path; // Visibility file (pattern)
    struct sep_kernel_data gridder, w_gridder; // uv/w gridder
    double shear_u, shear_v; // Shear factors - w' = shear_u*u + shear_v*v + w

    // Worker configuration
    int facet_workers; // number of facet workers
    int facet_max_work; // work list length per worker
    int facet_count; // Number of facets
    struct facet_work *facet_work; // facet work list (2d array - worker x work)
    int subgrid_workers; // number of subgrid workers
    int subgrid_work_count; // work list length
    int subgrid_work_assigned; // number of work items assigned to workers
    struct subgrid_work *subgrid_work; // subgrid work list
    int iu_min, iu_max, iv_min, iv_max; // subgrid columns/rows

    // Recombination configuration
    struct recombine2d_config recombine;

    // Source configuration (if we are working from a "sky model")
    int source_count;
    double *source_xy; // Source image positions [produce_souce_count x xy]
    double *source_lmn; // Source sky positions [produce_souce_count x lmn]
    double *source_corr; // Grid correction at source positions
    double source_energy; // Mean energy sources add to every grid cell

    // Parameters
    int config_dump_baseline_bins;
    int config_dump_subgrid_work;
    int produce_parallel_cols;
    int produce_retain_bf;
    int produce_batch_rows;
    int produce_queue_length;
    int produce_threads;
    int vis_bls_per_task;
    int vis_subgrid_queue_length;
    int vis_task_queue_length;
    int vis_chunk_queue_length;
    int vis_writer_count;
    int vis_fork_writer;
    int vis_check_existing;
    int vis_checks, grid_checks;
    double vis_check_error, vis_max_error;
    int vis_round_to_wplane;
    int vis_extra_grid_stride;
    int vis_interleave_wplanes;

    // Statsd connection
    int statsd_socket;
    double statsd_rate;

    // Worker assignment thread
    pthread_t work_assign_thread;
    MPI_Comm assign_comm;
    int assign_rank;
};

// Return size of total grid in wavelengths
inline static double config_lambda(const struct work_config *cfg)
{
    return cfg->recombine.image_size / cfg->theta;
}
// Return size of a subgrid as fraction of the entire grid
inline static double config_xA(const struct work_config *cfg)
{
    return (double)cfg->recombine.xA_size / cfg->recombine.image_size;
}

double get_time_ns();

void config_init(struct work_config *cfg);
bool config_set(struct work_config *cfg,
                int image_size, int subgrid_spacing,
                char *pswf_file, double W,
                int yB_size, int yN_size, int yP_size,
                int xA_size, int xM_size, int xMxN_yP_size);
bool config_assign_work(struct work_config *cfg,
                        int facet_workers, int subgrid_workers);

void config_free(struct work_config *cfg);

void config_set_visibilities(struct work_config *cfg,
                             struct vis_spec *spec,
                             const char *vis_path,
                             int sg_step_w, int margin, double target_err);
bool config_set_degrid(struct work_config *cfg,
                       const char *gridder_path, double gridder_x0, int downsample,
                       const char *w_gridder_path, double w_gridder_x0);

bool config_set_statsd(struct work_config *cfg,
                       const char *node, const char *service);
void config_send_statsd(struct work_config *cfg, const char *stat);

void config_load_facets(struct work_config *cfg, const char *path_fmt, const char *hdf5);
void config_check_subgrids(struct work_config *cfg,
                           double threshold, double fct_threshold, double degrid_threshold,
                           const char *check_fmt, const char *check_fct_fmt,
                           const char *check_degrid_fmt, const char *hdf5);

void config_set_sources(struct work_config *cfg, int count, unsigned int seed);

struct bl_data *vis_spec_to_bl_data(struct vis_spec *spec, double *shear_u, double *shear_v);
void config_pixel_to_lmn(int il, int im, double lam, double su, double sv,
                         double *pl, double *pm, double *pn);
bool create_bl_groups(hid_t vis_group, struct work_config *work_cfg, int worker);

struct subgrid_work *find_subgrid_work(struct work_config *cfg, int worker, int work);
void config_request_work(struct work_config *work_cfg, int *worker);
bool config_dynamic_work_assignment(struct work_config *work_cfg, int work_assign_rank);

int producer(struct work_config *wcfg, int facet_worker, int *streamer_ranks);
int streamer(struct work_config *wcfg, int subgrid_worker, int *producer_ranks);

#endif // CONFIG_H
